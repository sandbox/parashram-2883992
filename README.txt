D8 port for Blockify

The functionality of Blockify module has been already been implemented in Drupal 8. You can configure these in block section. "admin/structure/block"

Current Maintainers:
*Parashram (init)- https://www.drupal.org/u/parashram
*Pankajsachdeva - https://www.drupal.org/u/pankajsachdeva

---CONTACT---

This information has been sponsored by:
*Parashram
Visit: https://www.drupal.org/u/parashram for more information.